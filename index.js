const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
require('dotenv').config()

var corsOptions = {
    origin: "http://localhost:3000"
};

app.unsubscribe(cors(corsOptions))

app.use(express.json())

app.use(express.urlencoded({ extended: true}))

const db = require("./app/models");
db.sequelize.sync()

// ============================= WARNING!!! This will delete all your database ===================
// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });


app.get("/", (req, res) => {
    res.json({ message: "Welcome to Propen API" });
});


require('./app/routes/auth.routes')(app)

const PORT = process.env.PORT || 4000
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
})