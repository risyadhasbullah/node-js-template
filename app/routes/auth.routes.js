module.exports = app => {
    const auth = require('../controllers/auth.controller')
    let router = require('express').Router()

    // Create a new User
    router.post('/register', auth.create)

    // Login
    router.post('/login', auth.findOne)

    app.use('/api/auth', router)
}