const crypto = require('crypto')
module.exports = (sequelize, Sequelize) => {

    const Users = sequelize.define("users", {
      email: {
        type: Sequelize.STRING,
        required: true,
        lowercase: true,
        unique: true
      },
      password: {
        type: Sequelize.STRING,
        required: true,
        get() {
            return () => this.getDataValue('password')
        }
      },
      salt: {
          type: Sequelize.STRING,
        get() {
            return() => this.getDataValue('salt')
        }
      },
      fullName: {
        type: Sequelize.STRING,
        required: true,
      },
      createdAt: {
        type: Sequelize.DATE,
      }
    });

    Users.generateSalt = () => {
        return crypto.randomBytes(16).toString('base64')
    }

    Users.encryptPassword = (password, salt) => {
      return crypto
        .createHash('RSA-SHA256')
        .update(password)
        .update(salt)
        .digest('hex')
    }

    Users.validPassword = async(password, salt, hashedPassword) => {
      let hashPassword = await this.encryptPassword(password, salt)
      return hashPassword === hashedPassword
    }

    const setSaltAndPassword = user => {
        if(user.changed('password')){
            user.salt = Users.generateSalt()
            user.password = Users.encryptPassword(user.password(), user.salt())
        }
    }

    Users.beforeCreate(setSaltAndPassword)
    Users.beforeUpdate(setSaltAndPassword)
  
    return Users;
};