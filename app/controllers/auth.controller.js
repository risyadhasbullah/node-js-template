const db = require('../models')
const Users = db.users
const Op = db.Sequelize.Op
const crypto = require('crypto')

exports.create = (req, res) => {

    // Create a User
    const user = {
        email: req.body.email,
        password: req.body.password,
        fullName: req.body.fullName,
        createdAt: new Date(),
        updatedAt: new Date()
    }

    // Save User in the database
    Users.create(user)
    .then(data => {
        res.send(data)
    })
    .catch(err => {
        console.log(err)
        res.status(500).send({
            message:
            err.message || "Some error occurred while creating the Users."
        })
    })
}

exports.findOne = async (req, res) => {
    try{
        const {email, password} = req.body
        const user = (await Users.findOne({where: {email: email}})).dataValues
        
        if(!user) {
            res.status(500).send({
                message: "Email not found"
            });
        }
        if(!Users.validPassword){
            res.status(500).send({
                message: "Wrong password"
            });
        }
        const userRespone = new Users
        userRespone.id = user.id
        userRespone.email = user.email
        userRespone.fullName = user.fullName
        userRespone.createdAt = user.createdAt
        userRespone.updatedAt = user.updatedAt

        res.status(200).send(userRespone)
    } catch(error){
        res.status(500).send(error.message)
    }

}